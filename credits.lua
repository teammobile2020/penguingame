
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------


local function gotoMenu()--funzione per tornare al menu che abbiamo invocato alla linea 108 tramite il bottone menu
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

	local sceneGroup = self.view
	
	--ora creiamo la scena con gli highScores
	local cielo = display.newImageRect(sceneGroup,"sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
	cielo.x = display.contentCenterX;
	cielo.y = display.contentCenterY;

	local riquadro = display.newImage(sceneGroup,"pulsanti/3.png")
	riquadro.x = display.contentCenterX*1.08;
	riquadro.y = display.contentCenterY;
	riquadro.xScale = 0.26
	riquadro.yScale = 0.24

	local pallino = display.newImage(sceneGroup,"credits/13.png")
    pallino.x = display.contentCenterX * 1.7
	pallino.y = display.contentCenterY * 0.53
	pallino.xScale = 0.2
	pallino.yScale = 0.2

	local libro = display.newImage(sceneGroup,"credits/libro.png")
    libro.x = display.contentCenterX * 1.7
	libro.y = display.contentCenterY * 0.52
	libro.xScale = 0.44
	libro.yScale = 0.44

	local freccia = display.newImage(sceneGroup,"pulsanti/16.png")
    freccia.x = display.contentCenterX / 5.5
	freccia.y = display.contentCenterY * 0.4
	freccia.xScale = 0.1
	freccia.yScale = 0.1

	local alessia = display.newText(sceneGroup,"alessia marino", display.contentCenterX,display.contentCenterY*0.8, "pulsanti/scritta.otf", 24 )

	local antonio = display.newText(sceneGroup,"antonio giordano", display.contentCenterX,display.contentCenterY*1.2, "pulsanti/scritta.otf", 23 )


	local menuTesto = display.newText(sceneGroup,"menu", freccia.x+3, freccia.y-2, "pulsanti/scritta.otf", 10 )

	freccia:addEventListener( "tap", gotoMenu )
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		composer.removeScene( "credits" ) --per nascondere la scena highScores

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
