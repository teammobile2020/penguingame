local composer = require( "composer" )
 
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 

 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------


--torna al menu
local function gotoMenu()
     audio.play( suonoBottone )
     composer.removeScene("Database.classifica")
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end


local t={}

local punteggio = composer.getVariable( "finalScore" ) 


 -- create()
function scene:create( event )

	local sceneGroup = self.view

suonoBottone = audio.loadSound( "suoni/suonoBottone.wav" )
audio.setVolume( 0.3, suonoBottone)


--crea la scena

local db=require("Database.database")
      db.inizializza()

    local cielo = display.newImageRect(sceneGroup, "sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
	cielo.x = display.contentCenterX;
	cielo.y = display.contentCenterY; 

    local riquadro = display.newImage(sceneGroup,"pulsanti/3.png")
    riquadro.x = display.contentCenterX * 1.08;
	riquadro.y = display.contentCenterY; 
	riquadro.xScale = 0.26
	riquadro.yScale = 0.24

	local pallino = display.newImage(sceneGroup,"pulsanti/12.png")
    pallino.x = display.contentCenterX * 1.7
	pallino.y = display.contentCenterY * 0.5
	pallino.xScale = 0.11
	pallino.yScale = 0.11

	local coppa = display.newImage(sceneGroup,"pulsanti/coppa.png")
    coppa.x = display.contentCenterX * 1.7
	coppa.y = display.contentCenterY * 0.49
	coppa.xScale = 0.06
	coppa.yScale = 0.06

	local freccia = display.newImage(sceneGroup,"pulsanti/16.png")
    freccia.x = display.contentCenterX / 5.5
	freccia.y = display.contentCenterY * 0.4
	freccia.xScale = 0.1
	freccia.yScale = 0.1
	freccia:addEventListener( "tap", gotoMenu )

 	local highScoresHeader = display.newText(sceneGroup,"High Scores", display.contentCenterX, 100, "pulsanti/scritta.otf", 30)
	local menuTesto = display.newText( sceneGroup, "Menu",  freccia.x+3, freccia.y-2 , "pulsanti/scritta.otf", 10)

 end


-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
	
	--Scroll view
	local widget = require( "widget" )
  
-- ScrollView listener
local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end
 
-- Create the widget
local scrollView = widget.newScrollView(
    {
        top = 120,
        left = 40,
        width = 350,
        height = display.contentHeight*0.4,
        scrollWidth = display.contentWidth*0.8,
        scrollHeight = 0,
        listener = scrollListener,
		horizontalScrollDisabled=true,
		hideBackground=true,
    }
)

-- Create a image and insert it into the scroll view
--local background = display.newRect( display.contentCenterX,display.contentCenterY, 768, 2048 )
--background.fill={48/255,95/255,114/255}
--scrollView:insert( background )
sceneGroup:insert(scrollView)
--Stampa db
	
	local sqlite3=require("sqlite3")
	local path=system.pathForFile("penguin.db",system.DocumentsDirectory)
	local db=sqlite3.open(path)
	local cont=1
	for row in db:nrows("SELECT* FROM classifica") do
		table.insert(t,row)
		end
		
		local function compare (a,b)
			return (a.punteggio>b.punteggio)
		end
		table.sort(t,compare)
		for k,row in pairs(t) do
		local posizione=display.newText(cont..".",display.contentCenterX*0.35,20*cont*2,"pulsanti/Clafoutis Regular.ttf",22)
		
		posizione.fill={1,1,1}
		local giocatore=display.newText(row.nome,display.contentCenterX*0.8,20*cont*2,"pulsanti/scritta.otf",22)
		giocatore.fill={1,1,1}

		local punteggioScritta=display.newText(row.punteggio,display.contentCenterX*1.3,20*cont*2,"pulsanti/Clafoutis Regular.ttf",22)
		punteggioScritta.fill={1,1,1}

		punteggioScritta.text = string.format( "%0.1f",row.punteggio)
		scrollView:insert(posizione)
		scrollView:insert(giocatore)
		scrollView:insert(punteggioScritta)
		cont=cont+1
    end
	
	
	
end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        button:removeEventiListener("tap",press)
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene