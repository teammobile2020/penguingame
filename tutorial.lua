

local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- Initialize variables
local lives = 3
local score = 0
--local died = false
 
local livesText
local scoreText

local tutorialCuore
local tutorialTartaruga
local tutorialFuoco
local tutorialGolem

local sfondoGroup  --sfondo
local mainGroup --pingu
local uiGroup    --tasti

local suonoBottone
---------------------------------------------------------------------------------------

local datiFoglio5 = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 352/8,--lunghezza del signolo frame
  height = 26,--altezza del singolo frame
  numFrames = 8,
  sheetContentWidth = 352,--lunghezza totale function
  sheetContentheight = 26--idem
}

local datiFoglioGolem = {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 900,--lunghezza del signolo frame
  height = 900,--altezza del singolo frame
  numFrames = 12,
  sheetContentWidth = 2702,--lunghezza totale function
  sheetContentheight = 3603--idem
}

local datiFoglioFuoco= {--variabile tipo una struc-->indico le caratteristiche del foglio(sheet) con i frame

  width = 26,--lunghezza del signolo frame
  height = 10,--altezza del singolo frame
  numFrames = 60,
  sheetContentWidth = 156,--lunghezza totale function
  sheetContentheight = 100--idem
} 

local datiTarta2 = {
  { 
    name = "tarta2",
    start = 1,
    count = 8,
    time = 600,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}


local datiGolem = {
  { 
    name = "golem corsa",
    start = 1,
    count = 12,
    time = 1000,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}

local datiFuoco = {
  { 
    name = "fuoco",
    start = 1,
    count = 60,
    time = 900,
    loopCount = 0,
    loopDirection = "ended"
    
  }

}

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
function scene:create( event )

local sceneGroup = self.view

suonoBottone = audio.loadSound( "suoni/suonoBottone.wav" )
audio.setVolume( 0.3, suonoBottone)

local posi =  display.actualContentWidth / 2

    -- Set up display groups
    sfondoGroup = display.newGroup()  
    sceneGroup:insert( sfondoGroup )  
 
    mainGroup = display.newGroup()  
    sceneGroup:insert( mainGroup )  
 
    uiGroup = display.newGroup()   
    sceneGroup:insert( uiGroup )    

--torna al menu
local function gotoMenu()
    composer.gotoScene( "menu", { time=900, effect="crossFade" } )
end


-------------------------------------------------------RETTANGOLI------------------------------------------------------------

local salto = display.newRect( uiGroup,400, 200, 400, 200)
salto.x = display.contentCenterX -200;
salto.y = display.contentCenterY-23;
salto.alpha = 0.7;
salto:setFillColor( 0.7, 0, 0 )

local lampeggiaSalto = transition.blink( salto, { time=2000 } )
local function lampeggia( )
  transition.cancel( lampeggiaSalto )
end

timer.performWithDelay(lampeggia, 15)

local giu = display.newRect( uiGroup, 400, 200, 400, 200)
giu.x = display.contentCenterX -200;
giu.y = display.contentCenterY+180;
giu.alpha = 0;
giu:setFillColor( 1, 0.8, 0.2 )

local spara = display.newImage(uiGroup, "pulsanti/spara.png")
spara.x = display.contentCenterX *1.8
spara.y = display.actualContentHeight * 1.0
spara.xScale = 0.4
spara.yScale = 0.4
spara.alpha = 0

local quadratoGolem = display.newRect( uiGroup, 350, 110, 350, 110)
quadratoGolem.x = display.contentCenterX *1.2;
quadratoGolem.y = display.actualContentHeight/1.9
quadratoGolem.alpha = 0;
quadratoGolem:setFillColor( 0, 0.8, 0.8 )


-------------------------------------------------------SFONDO------------------------------------------------------------
local cielo = display.newImageRect(sfondoGroup, "sfondi/Sky.png",display.actualContentWidth+3, display.actualContentHeight)
cielo.x = display.contentCenterX;
cielo.y = display.contentCenterY;
cielo.speed = 0.8

local montagne = display.newImageRect(sfondoGroup,"sfondi/BG.png",display.actualContentWidth, display.actualContentHeight)
montagne.x = display.contentCenterX;
montagne.y = display.contentCenterY/0.95;
montagne.speed = 1

local alberi= display.newImageRect(sfondoGroup,"sfondi/Middle.png",display.actualContentWidth, display.actualContentHeight)
alberi.x = display.contentCenterX;
alberi.y = display.contentCenterY/0.88; 
alberi.speed = 2

local pavi1 = display.newImageRect(sfondoGroup,"sfondi/terra.png",display.actualContentWidth+3, display.actualContentHeight)
pavi1.x = display.contentCenterX;
pavi1.y = display.contentCenterY*1.1;
pavi1.speed = 3


-- Display lives and score

local sound = display.newImage(uiGroup,"pulsanti/sound.png")
sound.x = display.contentCenterX + posi/1.2
sound.y = display.actualContentHeight * 0.25
sound.xScale = 0.06
sound.yScale = 0.06

local noSound = display.newImage(uiGroup,"pulsanti/noSound.png")
noSound.x = display.contentCenterX + posi/1.2
noSound.y = display.actualContentHeight * 0.25
noSound.xScale = 0.06
noSound.yScale = 0.06
noSound.alpha = 0

 local function soundOff( )
      if(noSound.alpha == 0) then
        audio.setVolume( 0, suonoMusica)
          noSound.alpha = 1;
        sound.alpha = 0;
   end
end
    sound:addEventListener( "tap", soundOff )


 local function soundOn( ) 
   if(noSound.alpha == 1) then
         audio.setVolume( 0.5, suonoMusica)
         noSound.alpha = 0;
         sound.alpha = 1;
 end
end


 noSound:addEventListener( "tap", soundOn )


local pausa = display.newImage(uiGroup,"pulsanti/pausa.png")
pausa.x = display.contentCenterX + posi/1.5
pausa.y = display.actualContentHeight * 0.25
pausa.xScale = 0.06
pausa.yScale = 0.06

local life = display.newImage(uiGroup, "pulsanti/life.png")
life.x = display.contentCenterX /4
life.y = display.actualContentHeight * 0.25
life.xScale = 0.06
life.yScale = 0.06

local metri = display.newImage(uiGroup, "pulsanti/metri.png")
metri.x = display.contentCenterX /2
metri.y = display.actualContentHeight * 0.25
metri.xScale = 0.08
metri.yScale = 0.08

livesText = display.newText( uiGroup, "Lives: " ..lives, display.contentCenterX /3.4, display.actualContentHeight * 0.25, "pulsanti/scritta.otf", 7.5 )
scoreText = display.newText( uiGroup, score, display.contentCenterX /2, display.actualContentHeight * 0.23, "pulsanti/Clafoutis Regular.ttf", 8 )
local mText = display.newText( uiGroup, "Mt" , display.contentCenterX /2, display.actualContentHeight * 0.26, "pulsanti/Clafoutis Regular.ttf", 7 )
livesText:setFillColor( 1, 1, 1 )
scoreText:setFillColor(1, 1, 1)
mText:setFillColor(1, 1, 1)

-------------------------------------------------------FRECCE------------------------------------------------------------
--sul cuore
local freccia = display.newImage(sceneGroup,"tutorial/next.png")
freccia.x = display.contentCenterX *1.8
freccia.y = display.contentCenterY  * 1.7
freccia.xScale = 0.07
freccia.yScale = 0.07

--sulla tartaruga
local freccia1 = display.newImage(sceneGroup,"tutorial/next.png")
freccia1.x = display.contentCenterX *1.8
freccia1.y = display.contentCenterY  * 1.7
freccia1.xScale = 0.07
freccia1.yScale = 0.07
freccia1.alpha = 0;

--sul fuoco
local freccia2 = display.newImage(sceneGroup,"tutorial/next.png")
freccia2.x = display.contentCenterX *1.8
freccia2.y = display.contentCenterY  * 1.7
freccia2.xScale = 0.07
freccia2.yScale = 0.07
freccia2.alpha = 0;

--tornaMenu
local freccia3 = display.newImage(sceneGroup,"pulsanti/16.png")
freccia3.x = display.contentCenterX *0.1
freccia3.y = display.contentCenterY  * 1.7
freccia3.xScale = 0.07
freccia3.yScale = 0.07
freccia3.alpha = 0;


---------------------------------------------------IMMAGINILINEE-------------------------------------------------------------------

local linea1 = display.newImage(uiGroup, "tutorial/freccia.png")
linea1.x = display.contentCenterX *1.8
linea1.y = display.actualContentHeight * 0.7
linea1.xScale = 0.09
linea1.yScale = 0.09
linea1.alpha = 0;

----------------------------------------------OGGETTI-----------------------------------------------------------------------
local pingu = display.newImage("tutorial/pingSolo.png")
pingu.x = display.contentCenterX /3
pingu.y = display.actualContentHeight * 0.78
pingu.xScale = 0.18
pingu.yScale = 0.18

local cuore = display.newImage("move/cuore.png")
cuore.x = display.contentCenterX *3
cuore.y = display.actualContentHeight * 0.5
cuore.xScale = 0.06
cuore.yScale = 0.06
cuore.alpha = 1;
transition.to( cuore, { time=3000, x = display.contentCenterX/2+175, y = cuore.y } )

--scrittaCuore
tutorialCuore = display.newText( uiGroup, "tap here to take heart \n and increase your life" ,display.contentCenterX /1.9, display.actualContentHeight * 0.45, "pulsanti/scritta.otf", 20 )
tutorialCuore:setFillColor(0, 0, 0)



local animaTarta2 = graphics.newImageSheet ("nemici/tarta2.png", datiFoglio5)
local tartaruga = display.newSprite( animaTarta2, datiTarta2 )
tartaruga.x = display.contentCenterX*2.5;
tartaruga.y = display.contentCenterX*0.98;
tartaruga.xScale = 0.8
tartaruga.yScale = 0.8
tartaruga.alpha=0;

local fuoco = graphics.newImageSheet ("nemici/fuoco.png", datiFoglioFuoco)
local fuocoAnimation = display.newSprite(fuoco, datiFuoco)
fuocoAnimation.x = display.contentCenterX * 2.3;
fuocoAnimation.y = display.contentCenterX * 0.85;
fuocoAnimation.xScale = 4;
fuocoAnimation.yScale = 4;
fuocoAnimation.alpha = 0;


local golem = graphics.newImageSheet ("nemici/golem.png", datiFoglioGolem)
local animaGolemCorsa = display.newSprite(golem, datiGolem)
animaGolemCorsa.x = display.contentCenterX *2.3;
animaGolemCorsa.y = display.contentCenterX*0.85;
animaGolemCorsa.xScale = 0.13;
animaGolemCorsa.yScale = 0.13;
animaGolemCorsa.alpha = 0;



------------------------------------------FUNZIONI---------------------------------------------------------------------------



--togliCuore mostraTarta
local function tartaruga( )
    display.remove(cuore)
    display.remove(freccia)
    display.remove(tutorialCuore)
    audio.play( suonoBottone )
    freccia1.alpha = 1;
    animaTarta2 = graphics.newImageSheet ("nemici/tarta2.png", datiFoglio5)
    tartaruga = display.newSprite( animaTarta2, datiTarta2 )
    tartaruga.x = display.contentCenterX*2.5;
    tartaruga.y = display.contentCenterX*0.98;
    tartaruga.xScale = 0.8
    tartaruga.yScale = 0.8
    tartaruga.alpha=1;
    tartaruga:play()
    transition.to( tartaruga, { time=3000, x = display.contentCenterX/2+170, y = tartaruga.y } )
    tutorialTartaruga = display.newText( uiGroup, "watch out for turtles \n tap here to jump" ,display.contentCenterX /1.9, display.actualContentHeight * 0.45, "pulsanti/scritta.otf", 20 )
    tutorialTartaruga:setFillColor(0, 0, 0)
    salto:setFillColor( 0.3, 0.7, 1 )
    
end


--togliTarta mostraFuoco
local function fuoco( )
    display.remove(salto)
     audio.play( suonoBottone )
    --display.remove(linea1)
    display.remove(tartaruga)
    display.remove(tutorialTartaruga)
    display.remove(freccia1)
    giu.alpha=0.7;

local lampeggiaGiu = transition.blink( giu, { time=2000 } )
local function lampeggia2( )
  transition.cancel( lampeggiaGiu )
end

 timer.performWithDelay( lampeggia2, 15)
    freccia2.alpha=1;
    tutorialFuoco = display.newText( uiGroup, "be careful with fire \n  tap here to crouch" ,display.contentCenterX /2, display.actualContentHeight , "pulsanti/scritta.otf", 20)
    tutorialFuoco:setFillColor(0, 0, 0)
    fuocoAnimation.alpha = 1;
    fuocoAnimation:play()
    transition.to( fuocoAnimation, { time=2700, x = display.contentCenterX/2+230, y = fuocoAnimation.y } )
   
     freccia2.alpha = 1;
end

----togliFuoco mostraGolem 
local function golem( )
    display.remove(giu)
    display.remove(tutorialFuoco)
    display.remove(fuocoAnimation)
    audio.play( suonoBottone )
    linea1.alpha = 1;
    quadratoGolem.alpha = 0.7;
    spara.alpha = 1;
    freccia3.alpha = 1;
    animaGolemCorsa.alpha = 1;
    animaGolemCorsa:play()
    transition.to( animaGolemCorsa, { time=3000,x = display.contentCenterX/2+200, y = animaGolemCorsa.y} )
    tutorialGolem = display.newText( uiGroup, "use the shotgun against golems \n click here to shoot" , display.contentCenterX *1.2, display.actualContentHeight/1.9 , "pulsanti/scritta.otf", 19)
    tutorialGolem:setFillColor(0, 0, 0)
    display.remove(freccia2)
    local menuTesto = display.newText( sceneGroup, "Menu",  freccia3.x+3, freccia3.y-2 , "pulsanti/scritta.otf", 10)
end

local function distruggi()
    audio.play( suonoBottone )
  display.remove(quadratoGolem)
  display.remove(animaGolemCorsa)
  display.remove(pingu)
  display.remove(tutorialGolem)
  display.remove(linea1)
     
end

freccia:addEventListener( "tap", tartaruga )
freccia1:addEventListener( "tap", fuoco )
freccia2:addEventListener( "tap", golem )
freccia3:addEventListener( "tap", gotoMenu )
freccia3:addEventListener( "tap", distruggi )


end





----------------------------------------------------------------------------------------------------------------------------------------------------------
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
        
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
          

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
  
       composer.removeScene( "tutorial" )
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
